// Helper Functions
export const Distance3D = (p0, p1) => {
    const dVector = new THREE.Vector3(p0.x - p1.x, p0.y - p1.y, p0.z - p1.z)
    return Math.sqrt(dVector.x * dVector.x + dVector.y * dVector.y + dVector.z * dVector.z)
};
  
export const PointOnALine3D = (p0, p1, d) => {
    const length = Distance3D(p0, p1)
    const vector = new THREE.Vector3(p1.x - p0.x, p1.y - p0.y, p1.z - p0.z)
    const nVector = new THREE.Vector3(vector.x / length, vector.y / length, vector.z / length)
    const poal = new THREE.Vector3(p0.x + d * nVector.x, p0.y + d * nVector.y, p0.z + d * nVector.z)
    return poal
};