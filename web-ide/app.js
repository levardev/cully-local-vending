// Copyright (c) 2021 8th Wall, Inc.
//
// app.js is the main entry point for your 8th Wall app. Code here will execute after head.html
// is loaded, and before body.html is loaded.
import './index.css'
import {clippingPlaneScenePipelineModule} from './threejs-scene-init'
import {LevarApp} from './splash-image'
import * as camerafeedHtml from './camerafeed.html'

const AppMain = new LevarApp()

const onxrloaded = (e) => {
//   const splashimage = document.getElementById('splashimage')
//   splashimage.style.display = 'none'
  // Add a canvas to the document for our xr scene.
  // document.body.insertAdjacentHTML('beforeend', camerafeedHtml)
  // Open the camera and start running the camera run loop.
  document.body.insertAdjacentHTML('beforeend', camerafeedHtml)

  XR8.addCameraPipelineModules([  // Add camera pipeline modules.
    // Existing pipeline modules.
    XR8.GlTextureRenderer.pipelineModule(),      // Draws the camera feed.
    XR8.Threejs.pipelineModule(),                // Creates a ThreeJS AR Scene.
    XR8.XrController.pipelineModule(),           // Enables SLAM tracking.
    window.LandingPage.pipelineModule(),         // Detects unsupported browsers and gives hints.
    XRExtras.FullWindowCanvas.pipelineModule(),  // Modifies the canvas to fill the window.
    XRExtras.Loading.pipelineModule(),           // Manages the loading screen on startup.
    XRExtras.RuntimeError.pipelineModule(),      // Shows an error image on runtime error.
    // Custom pipeline modules.
    clippingPlaneScenePipelineModule(),  // Sets up the threejs camera and scene content.
  ])

  XR8.run({
    canvas: document.getElementById('camerafeed'),
    allowedDevices: XR8.XrConfig.device().ANY,
  })
}

// window.addEventListener('load', (event) => {
//   AppMain.start(onxrloaded)
// });
XRExtras.Loading.showLoading({onxrloaded})

// Show loading screen before the full XR library has been loaded.
// 
// window.XR8 ? window.addEventListener('xrloaded', ) : alert('hellow')