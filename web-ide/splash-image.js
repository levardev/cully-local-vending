export class LevarApp {
    constructor() {
      // this.init()
      this.eightWall = 'test string'
    }
  
    start(onxrloaded) {
      const start = document.getElementById('start')
      start.addEventListener('click', function(){
        XRExtras.Loading.showLoading({onxrloaded})
      }, false);
    }
  
    testEnvironment() {
      return this.eightWall
    }
};
  