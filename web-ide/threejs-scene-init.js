//
// can needs explicit lights // BEN
// EXTRA
// landing page
// mixer.time send can earlier
// tween out hero bottles // clipless on tween
// Loading all 4 cans beforehand
// TWEEN Opacity
//

/* globals TWEEN */
import {Distance3D, PointOnALine3D} from './helper'
import {TouchControl} from './touch-controls'
const touchControl = new TouchControl()

let isXRSceneInit = false
let isGhostPlaced = false
let isModelLoaded = false
let isHeroCanVisible = false
let group; let promptUI; let placeUI; let resetBtn

const clippingPlaneScenePipelineModule = () => {
  const mainGLBfile = require('./assets/vending/Shipyard_Fast.glb')
  const pumpkinHeadGLB = require('./assets/vending/Pumpkin_Head.glb')
  const blueBerryGLB = require('./assets/vending/BlueBerry.glb')
  const brownAleGLB = require('./assets/vending/Brown_Ale.glb')
  const exportGLB = require('./assets/vending/Export_Ale.glb')
  const helperOpacMap = require('./assets/vending_helper_opacity.jpg')
  const groundOpacMap = require('./assets/vending_placement_opacity.jpg')
  const loader = new THREE.GLTFLoader()
  const startScale = new THREE.Vector3(0.8, 0.8, 0.8)
  const endScale = new THREE.Vector3(1, 1, 1)

  const vendingMachineAnimationMillis = 2000
  const canAnimationMillis = 3500

  promptUI = document.getElementById('promptUI')
  placeUI = document.getElementById('placeUI')
  resetBtn = document.getElementById('resetButton')

  const raycaster = new THREE.Raycaster()
  const tapPosition = new THREE.Vector2()

  const textureLoader = new THREE.TextureLoader()
  const clock = new THREE.Clock()
  const rayOrigin = new THREE.Vector2(0, 0)
  const cursorLocation = new THREE.Vector3(0, 0, 0)

  const ghostGeometry = new THREE.BoxGeometry(1, 1, 1)
  const ghostMaterial = new THREE.MeshBasicMaterial({color: 0x00ff00})
  const ghostCube = new THREE.Mesh(ghostGeometry, ghostMaterial)

  let model; let mixer

  let cube0; let cube1; let cube2; let cube3
  let surface
  let canModel
  let pointXCurr; let pointZCurr
  let canStartRaycast; let canStopCamera

  let helperPlane
  let groundPlane

  let blueBerryAction; let exportAction; let pumpkinAction; let brownAction

  const freezeVendingMachine = () => {
    blueBerryAction.play()
    mixer.setTime(0)
    blueBerryAction.paused = true
  }

  const stopAllActions = () => {
    blueBerryAction.stop()
    exportAction.stop()
    pumpkinAction.stop()
    brownAction.stop()
  }

  const introduceVending = () => {
    if (!model) return
    const scale = {...startScale}
    const {scene} = XR8.Threejs.xrScene()
    model.children[0].children[1].material.transparent = false
    model.children[0].children[1].material.opacity = 1

    XR8.Threejs.xrScene().scene.traverse((node) => {
      if (node.name === 'directionalShadow') {
        node.castShadow = true
      }
    })

    console.log(XR8.Threejs.xrScene().scene)
    new TWEEN.Tween(scale)
      .to(endScale, vendingMachineAnimationMillis)
      .easing(TWEEN.Easing.Elastic.Out)
      .onUpdate(() => {
        model.scale.set(scale.x, scale.y, scale.z)
      })
      .onComplete(() => {
        promptUI.style.display = 'flex'
        resetBtn.style.display = 'block'
        helperPlane.visible = true
      })
      .start()
  }

  function tweenInCan(canGLBModel, duration) {
    const {scene, camera} = XR8.Threejs.xrScene()
    camera.add(canGLBModel)
    console.log('tweenInCan 1d')
    const scale = {...startScale}
    canGLBModel.scale.set(scale.x, scale.y, scale.z)
    isHeroCanVisible = true
    canGLBModel.rotation.y = -0.5
    const fillLight = new THREE.AmbientLight(0xf4fdff, 0.35)
    
    camera.add(fillLight)
    fillLight.position.set(0, 0, -1)
    fillLight.target = camera
    
    new TWEEN.Tween(scale)
      .to(endScale, duration)
      .easing(TWEEN.Easing.Quadratic.InOut)
      .onUpdate(() => {
        canGLBModel.scale.set(scale.x, scale.y, scale.z)
        canGLBModel.position.set(0, 0, -0.2)
      })
      .onComplete(() => {
        // update exit for only hero can
        console.log('tweenInCan Complete')
      })
      .start(console.log('tweenInCan Start'))
  }

  const handleHeroCan = (canGLBModel) => {
    if (!canGLBModel) return
    console.log('handleHeroCan 1c')
    const {scene, camera} = XR8.Threejs.xrScene()
    tweenInCan(canGLBModel, canAnimationMillis)
  }

  const loadHeroCan = (canName) => {
    console.log('loadHeroCan 1b')
    freezeVendingMachine()

    let ASSETTOLOAD

    switch (canName) {
      case 'Anim2':
        ASSETTOLOAD = exportGLB
        break
      case 'Anim1':
        ASSETTOLOAD = blueBerryGLB
        break
      case 'Anim4':
        ASSETTOLOAD = brownAleGLB
        break
      case 'Anim3':
        ASSETTOLOAD = pumpkinHeadGLB
        break
      default:
        ASSETTOLOAD = blueBerryGLB
        break
    }

    loader.load(ASSETTOLOAD, (canGlb) => {
      canModel = canGlb.scene
      canModel.name = 'current_can'
      console.log('loader 1bb')
      handleHeroCan(canModel)
    })
  }

  // load the hologram at the requested point on the surface
  const placeModel = () => {
    if (!isGhostPlaced) {
      group = new THREE.Group()
      group.name = 'levarGroup'

      cube0 = new THREE.Mesh(
        new THREE.BoxGeometry(0.2, 0.2, 0.05),
        new THREE.MeshBasicMaterial({color: 0x00ff00, visible: false})
      )
      cube1 = new THREE.Mesh(
        new THREE.BoxGeometry(0.2, 0.2, 0.05),
        new THREE.MeshBasicMaterial({color: 0x00ff00, visible: false})
      )
      cube2 = new THREE.Mesh(
        new THREE.BoxGeometry(0.2, 0.2, 0.05),
        new THREE.MeshBasicMaterial({color: 0x00ff00, visible: false})
      )
      cube3 = new THREE.Mesh(
        new THREE.BoxGeometry(0.2, 0.2, 0.05),
        new THREE.MeshBasicMaterial({color: 0x00ff00, visible: false})
      )

      cube0.name = 'blueBerry'
      cube1.name = 'export'
      cube2.name = 'pumpkin'
      cube3.name = 'brown'

      // group positions go here

      cube0.position.set(0.54, 1.37, 0.35)
      cube1.position.set(0.54, 1.1, 0.35)
      cube2.position.set(0.54, 0.85, 0.35)
      cube3.position.set(0.54, 0.57, 0.35)

      group.add(cube0); group.add(cube1); group.add(cube2); group.add(cube3)

      // BEGIN HELPER
      const opacTexture = THREE.ImageUtils.loadTexture(helperOpacMap)
      const groundTexture = THREE.ImageUtils.loadTexture(groundOpacMap)
      const material = new THREE.MeshStandardMaterial(
        {color: 0xffffff, transparent: true, alphaMap: opacTexture}
      )

      const groundMaterial = new THREE.MeshStandardMaterial(
        {color: 0xffffff, transparent: true, alphaMap: groundTexture}
      )

      helperPlane = new THREE.Mesh(new THREE.PlaneGeometry(0.55, 0.55), material)
      helperPlane.position.set(0.52, 0.4, 0.75)
      helperPlane.rotation.x = Math.PI / -5
      helperPlane.visible = false

      groundPlane = new THREE.Mesh(new THREE.PlaneGeometry(1.6, 1), groundMaterial)
      groundPlane.position.set(0, 0, 0)
      groundPlane.rotation.x = Math.PI / -2

      group.add(helperPlane)
      group.add(groundPlane)
      // END HELPER

      // BEGIN LIGHTING
      const spotLight = new THREE.DirectionalLight(0xffffff, 0.05)
      const spotLight2 = new THREE.DirectionalLight(0xffffff, 0.1)  // original settings
      const directionalShadow = new THREE.SpotLight(0xffffff, 0.5)
      const keyLight = new THREE.RectAreaLight(0xffd6aa, 1.8, 10, 10)
      const fillLight = new THREE.RectAreaLight(0xf4fdff, 1.6, 10, 10)
      const backLight = new THREE.RectAreaLight(0xffd6aa, 1.3, 10, 10)
      const lightTarget = new THREE.Object3D()
      const lightTarget2 = new THREE.Object3D()

      spotLight.position.set(4, 3, 4)
      spotLight2.position.set(-2, 0.2, 2) 
      directionalShadow.position.set(3, 7.0, 1.25)

      keyLight.position.set(3, 0.5, 4)
      fillLight.position.set(-6, 1, 8)
      backLight.position.set(-4, 0.5, -8)

      keyLight.lookAt(0, 1.25, 0)
      backLight.lookAt(0, 1.25, 0)
      fillLight.lookAt(0, 0.6, 0)

      lightTarget.position.set(0, 0, 0)
      lightTarget2.position.set(0, 1.4, 0)

      spotLight.target = lightTarget
      spotLight2.target = lightTarget
      directionalShadow.target = lightTarget2

      directionalShadow.shadow.camera.near = 0.1
      directionalShadow.shadow.camera.far = 5000
      directionalShadow.shadow.camera.right = 30
      directionalShadow.shadow.camera.left = -30
      directionalShadow.shadow.camera.top = 30
      directionalShadow.shadow.camera.bottom = -30
      directionalShadow.shadow.mapSize.width = 2048
      directionalShadow.shadow.mapSize.height = 2048
      directionalShadow.name = 'directionalShadow'

      // CAMERA HELPER FOR spotLight2
      // const cameraHelper2 = new THREE.CameraHelper(spotLight2.shadow.camera)
      // XR8.Threejs.xrScene().scene.add(cameraHelper2)

      // const cameraHelperShadow = new THREE.CameraHelper(directionalShadow.shadow.camera)
      // XR8.Threejs.xrScene().scene.add(cameraHelperShadow)

      group.add(keyLight)
      group.add(fillLight)
      group.add(backLight)
      group.add(lightTarget)
      group.add(lightTarget2)
      group.add(spotLight2)
      group.add(directionalShadow)
      // XR8.Threejs.xrScene().scene.add(directionalShadow)
      // END LIGHTING

      loader.load(mainGLBfile, (gltf) => {
        gltf.scene.traverse((node) => {
          if (node.isMesh) {
            node.castShadow = true
          }
        })
        model = gltf.scene
        model.children[0].children[1].material.transparent = true
        model.children[0].children[1].material.opacity = 0

        const clips = gltf.animations || []

        mixer = new THREE.AnimationMixer(model)
        blueBerryAction = mixer.clipAction(clips[0])
        blueBerryAction.loop = THREE.LoopOnce
        exportAction = mixer.clipAction(clips[1])
        exportAction.loop = THREE.LoopOnce
        pumpkinAction = mixer.clipAction(clips[2])
        pumpkinAction.loop = THREE.LoopOnce
        brownAction = mixer.clipAction(clips[3])
        brownAction.loop = THREE.LoopOnce

        freezeVendingMachine()

        mixer.addEventListener('finished', (e) => {
          console.log('finished 1a')
          loadHeroCan(e.action._clip.name)
          mixer.setTime(0)
        })

        group.add(model)
        XR8.Threejs.xrScene().scene.add(group)
        isModelLoaded = true
      })
      placeUI.style.display = 'none'
    }
  }

  const placeModelHandler = (e) => {
    console.log('Hellow', isXRSceneInit, isGhostPlaced, isModelLoaded)
    if (!isXRSceneInit || isGhostPlaced || !isModelLoaded) return
    const {scene, camera} = XR8.Threejs.xrScene()
    isGhostPlaced = true
    introduceVending(model)
    console.log(camera.position)
    const groundCamera = new THREE.Vector3(camera.position.x, 0, camera.position.z)
    group.lookAt(groundCamera)
    groundPlane.visible = false
  }

  const placeObjectTouchHandler = (e) => {
    if (e.touches.length === 2) {
      XR8.XrController.recenter()
    }

    if (e.touches.length > 2) {
      return
    }

    const {scene, camera} = XR8.Threejs.xrScene()

    // calculate tap position in normalized device coordinates (-1 to +1) for both components.
    tapPosition.x = (e.touches[0].clientX / window.innerWidth) * 2 - 1
    tapPosition.y = -(e.touches[0].clientY / window.innerHeight) * 2 + 1

    // Update the picking ray with the camera and tap position.
    raycaster.setFromCamera(tapPosition, camera)
    const intersects = raycaster.intersectObjects([cube0, cube1, cube2, cube3])

    if (intersects.length > 0) {
      if (intersects[0].object.name === 'blueBerry') {
        helperPlane.visible = false
        stopAllActions()
        blueBerryAction.play()
      } else if (intersects[0].object.name === 'export') {
        helperPlane.visible = false
        stopAllActions()
        exportAction.play()
      } else if (intersects[0].object.name === 'pumpkin') {
        helperPlane.visible = false
        stopAllActions()
        pumpkinAction.play()
      } else if (intersects[0].object.name === 'brown') {
        helperPlane.visible = false
        stopAllActions()
        brownAction.play()
      }
    }
  }


  const placeGhostHandler = () => {
    const difference = function (a, b) { return Math.abs(a - b); }
    
    const getDistance = function (x1, y1, x2, y2) {
        let y = x2 - x1;
        let x = y2 - y1;
        
        return Math.sqrt(x * x + y * y);
    }

    const {scene, camera} = XR8.Threejs.xrScene()
    const cameraPosition = new THREE.Vector3()
    camera.getWorldPosition(cameraPosition)
    raycaster.setFromCamera(rayOrigin, camera)
    const intersects = raycaster.intersectObjects(scene.children)

    if (isModelLoaded) {
        if (intersects.length === 0){
            group.position.set(0, 0, 1)
            console.log('intersects.length === 0')            
        }
      else if (intersects[0].object === surface) {
        const differenceX = difference(cameraPosition.x,  intersects[0].point.x)
        const differenceZ = difference(cameraPosition.z,  intersects[0].point.z)
        const distance = getDistance(cameraPosition.x, cameraPosition.z, intersects[0].point.x, intersects[0].point.z)
        // console.log('distance', distance)
        if (distance >= 5) {

            // doesnt allow vending machine to go past the corners of the bounded square
            if (differenceX >= 5 && intersects[0].point.x > 0 && differenceZ >= 5 && intersects[0].point.z > 0) {
                group.position.set(5, 0, 5)
            }
            else if (differenceX >= 5 && intersects[0].point.x < 0 && differenceZ >= 5 && intersects[0].point.z < 0) {
                group.position.set(-5, 0, -5)
            }
            else if (differenceX >= 5 && intersects[0].point.x > 0 && differenceZ >= 5 && intersects[0].point.z < 0) {
                group.position.set(5, 0, -5)
            }
            else if (differenceX >= 5 && intersects[0].point.x < 0 && differenceZ >= 5 && intersects[0].point.z > 0) {
                group.position.set(-5, 0, 5)
            }
            else {
                // doesnt allow vending machine to go past the X-Edges of the bounded square
                if (differenceX >= 5 && intersects[0].point.x > 0) {
                    group.position.set(5, 0, intersects[0].point.z)
                }
                else if (differenceX >= 5 && intersects[0].point.x < 0) {
                    group.position.set(-5, 0, intersects[0].point.z)
                }
                
                // doesnt allow vending machine to go past the Z-Edges s of the bounded square
                if (differenceZ >= 5 && intersects[0].point.z > 0) {
                    group.position.set(intersects[0].point.x, 0, 5)
                }
                else if (differenceZ >= 5 && intersects[0].point.z < 0) {
                    group.position.set(intersects[0].point.x, 0, -5)
                }
            }
        }
        else {
            group.position.set(intersects[0].point.x, 0, intersects[0].point.z)
        }
        const groundCamera = new THREE.Vector3(camera.position.x, 0, camera.position.z)
        group.lookAt(groundCamera)
      }
    }
  }

  const resetArt = () => {
    stopAllActions()
    const {scene, camera} = XR8.Threejs.xrScene()
    if (isHeroCanVisible) {
      const currentCan = scene.getObjectByName('current_can')
      camera.remove(currentCan)
      isHeroCanVisible = false
      canModel = false
    } else {
      const currentGroup = scene.getObjectByName('levarGroup')
      model.children[0].children[1].material.transparent = true
      model.children[0].children[1].material.opacity = 0

      scene.traverse((node) => {
        if (node.name === 'directionalShadow') {
          node.castShadow = false
        }
      })

      currentGroup.position.set(0, 0, 0)
      groundPlane.visible = true
      helperPlane.visible = false

      promptUI.style.display = 'none'
      resetBtn.style.display = 'none'
      placeUI.style.display = 'block'
      isGhostPlaced = false
    }
  }

  const initXrScene = ({scene, camera, renderer}) => {
    renderer.shadowMap.enabled = true
    renderer.outputEncoding = THREE.sRGBEncoding
    renderer.shadowMap.type = THREE.PCFSoftShadowMap

    // surface = new THREE.Mesh(
    //   new THREE.CylinderGeometry( 10, 10, 1, 16 ),
    //   new THREE.MeshStandardMaterial(
    //     {color: 0xffff00}
    //   )
    // )

    surface = new THREE.Mesh(
      new THREE.CircleGeometry(10, 48),
      new THREE.ShadowMaterial(
        {opacity: 0.7}
      )
    )

    surface.name = 'scene_surface'
    surface.rotateX(-Math.PI / 2)
    surface.position.set(0, 0, 0)
    surface.receiveShadow = true
    scene.add(surface)
    // scene.add(new THREE.AmbientLight())
    placeModel()
    camera.position.set(0, 1.6, 0)
    isXRSceneInit = true

    // TEST CUBES
    // const geometry = new THREE.BoxGeometry(0.5, 10, 0.5)
    // const materialTest = new THREE.MeshStandardMaterial(
    //   {color: 0xffffff, transparent: true}
    // )
    // const cubeTestLeft = new THREE.Mesh(geometry, materialTest)
    // const cubeTestRight = new THREE.Mesh(geometry, materialTest)

    // cubeTestLeft.position.set(-1, 2, -3)
    // cubeTestLeft.castShadow = true
    // cubeTestLeft.receiveShadow = true

    // cubeTestRight.position.set(1, 2, -3)
    // cubeTestRight.castShadow = true
    // cubeTestRight.receiveShadow = true

    // scene.add(cubeTestLeft)
    // scene.add(cubeTestRight)

    resetBtn.addEventListener('click', resetArt)  // Add touch listener // might move
  }

  return {
    name: 'clipping-plane-scene',
    onStart: ({canvas}) => {
      const {scene, camera, renderer} = XR8.Threejs.xrScene()  // Get the 3js scene from XR8.Threejs
      initXrScene({scene, camera, renderer})  // Add objects set the starting camera position.
      // prevent scroll/pinch gestures on canvas
      canvas.addEventListener('touchmove', (event) => {
        event.preventDefault()
      })

      const animate = (time) => {
        requestAnimationFrame(animate)
        TWEEN.update(time)
      }
      animate()

      // Sync the xr controller's 6DoF position and camera paremeters with our scene.
      XR8.XrController.updateCameraProjectionMatrix({
        origin: camera.position,
        facing: camera.quaternion,
      })

      canvas.addEventListener('touchstart', placeObjectTouchHandler, true)
      canvas.addEventListener('touchstart', placeModelHandler, true)
    },
    onUpdate: () => {
      if (model) mixer.update(clock.getDelta())
      if (!isGhostPlaced) placeGhostHandler()
    },
  }
};

export {clippingPlaneScenePipelineModule}